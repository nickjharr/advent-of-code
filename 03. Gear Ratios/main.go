package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"unicode"
)

func main() {

	var grid [][]rune
	var total int
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		grid = append(grid, []rune(fileScanner.Text()))
	}

	for i, row := range grid {
		var value []rune
		var startIndex int = -1

		for j, cell := range grid[i] {

			adjacent := false
			if unicode.IsDigit(cell) {
				value = append(value, cell)

				if startIndex == -1 {
					startIndex = j
				}
			}

			if !unicode.IsDigit(cell) || j == len(grid[i]) - 1{
				if startIndex != -1 {

					leftOffset, rightOffset := 0, 0

					if startIndex > 0 {
						leftOffset = -1
					}

					if startIndex+len(value) < len(row) {
						rightOffset = 1
					}

					// check row above
					if i > 0 {
						start, end := startIndex + leftOffset, startIndex+len(value)+rightOffset

						for k := start; k < end; k++ {
							if grid[i-1][k] != '.' && !unicode.IsDigit(grid[i-1][k]) {
								adjacent = true
							}
						}
					}

					// check left
					if startIndex > 0 {
						if grid[i][startIndex-1] != '.' {
							adjacent = true
						}
					}

					// check right
					if startIndex+len(value) < len(row) {
						if grid[i][startIndex+len(value)] != '.' {
							adjacent = true
						}
					}

					// check row below
					if i < len(grid)-1 {

						start, end := startIndex + leftOffset, startIndex+len(value)+rightOffset

						for k := start; k < end; k++ {
							if grid[i+1][k] != '.' && !unicode.IsDigit(grid[i+1][k]) {
								adjacent = true
							}
						}
					}
				}

				if adjacent {
					intval, _ := strconv.ParseInt(string(value), 10, 32)
					total += int(intval)
				}

				startIndex = -1
				value = nil
			}
		}
	}

	fmt.Printf("%d", total)
}
