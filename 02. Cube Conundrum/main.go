package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main () {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)
	
	var runningTotal int64

	for fileScanner.Scan() {
		line := fileScanner.Text()
		
		games := strings.Split(line, ": ")

		gameNumber, _ := strconv.ParseInt(strings.Split(games[0], " ")[1], 10, 32)
		gameResults := strings.Split(games[1], "; ")
		
		valid := true

		for _, result := range gameResults {
			valid = checkPicks(result)

			if !valid {
				valid = false
				break
			}
		}
		
		if valid {
			runningTotal += gameNumber
		}
	}

	fmt.Println(runningTotal)
}

func checkPicks (game string) bool {	
	picks := strings.Split(game, ", ")

	var red, blue, green int64

	for _, pick := range picks {
				
		split := regexp.MustCompile(" +").Split(pick, 2)

		r, _ := strconv.ParseInt(split[0], 10, 32)
		if split[1] == "red" {
			red += r 
		} else if split[1] == "blue" {
			blue += r
		} else if split[1] == "green" {
			green += r
		}
	}

	return !(red > 12 || green > 13 || blue > 14)
}