package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"slices"
	"strings"
)

func main () {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	runningTotal := 0

	for fileScanner.Scan() {
		line := fileScanner.Text()
		
		cleaned := regexp.MustCompile(": +").Split(line, 2)[1];
		numbers := strings.Split(cleaned, " | ")

		winningNumbers := regexp.MustCompile(" +").Split(numbers[0], -1)
		ourNumbers := regexp.MustCompile(" +").Split(numbers[1], -1)

		lineTotal := 0

		for _, val := range ourNumbers {
			if slices.Contains(winningNumbers, val) {
				if lineTotal > 0 {
					lineTotal *= 2
				}else{
					lineTotal = 1
				}
			}
		}

		runningTotal += lineTotal
	}

	fmt.Println(runningTotal)
}