package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

func main() {
	var partOneRunningTotal, partTwoRunningTotal int

	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := []rune(fileScanner.Text())
		partOneRunningTotal += partOne(line)
		partTwoRunningTotal += partTwo(line)
	}
	readFile.Close()

	_, _ = fmt.Printf("Part 1: %d\n", partOneRunningTotal)
	_, _ = fmt.Printf("Part 2: %d", partTwoRunningTotal)

}

func partOne(line []rune) int {
	var firstDigit, lastDigit int

	for i, j := 0, len(line)-1; i < len(line); i, j = i+1, j-1{
		if unicode.IsDigit(line[i]) && firstDigit == 0 {
			firstDigit = int(line[i] - '0');
		}

		if unicode.IsDigit(line[j]) && lastDigit == 0 {
			lastDigit = int(line[j] - '0')
		}

		if firstDigit != 0 && lastDigit != 0 {
			return firstDigit * 10 + lastDigit
		}
	}

	return 0
}

func partTwo(line []rune) int {

	var keywords = []string{
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
	}

	firstNumericValue, firstNumericIndex := getFirstDigitWithIndex(line)

	firstStringValue, firstStringIndex := getFirstStringNumberWithIndex(line, keywords)

	var firstNumber int
	if firstNumericIndex < firstStringIndex || firstStringIndex == -1 {
		firstNumber = firstNumericValue
	} else {
		firstNumber = firstStringValue
	}

	lastNumericValue, lastNumericIndex := getLastDigitWithIndex(line)

	// create a slice of the string version of the numbers, but reversed
	var reversedKeywords []string
	for _, word := range keywords {
		reversedKeywords = append(reversedKeywords, string(reverseString(word)))
	}

	lastStringValue, lastStringIndex := getLastStringNumberWithIndex(line, reversedKeywords)

	var lastNumber int
	if lastNumericIndex > lastStringIndex {
		lastNumber = lastNumericValue
	} else {
		lastNumber = lastStringValue
	}

	lineSolution := firstNumber * 10 + lastNumber

	return lineSolution
}

func getFirstDigitWithIndex(chars []rune) (int, int) {

	for i, c := range chars {
		if unicode.IsDigit(c) {
			return int(c - '0'), i
		}
	}

	return -1, -1
}

func getLastDigitWithIndex(chars []rune) (int, int) {
	for i := len(chars) - 1; i >= 0; i-- {
		if unicode.IsDigit(chars[i]) {
			return int(chars[i] - '0'), i
		}
	}

	return -1, -1
}

func getFirstStringNumberWithIndex(line []rune, keywords []string) (int, int) {
	firstStringIndex := -1
	var firstStringValue int

	for i, word := range keywords {
		substringIndex := strings.Index(string(line), word)

		if substringIndex >= 0 && (substringIndex <= firstStringIndex || firstStringIndex == -1) {
			firstStringIndex = substringIndex
			firstStringValue = i + 1
		}
	}

	return firstStringValue, firstStringIndex
}

func getLastStringNumberWithIndex(line []rune, keywords []string) (int, int) {

	var lastStringIndex, lastStringValue int = -1, 0

	for j, word := range keywords {
		reversedLine := reverseString(string(line))

		substringIndex := strings.Index(reversedLine, word)
		trueIndex := len(line) - (substringIndex + len(word))

		if trueIndex >= 0 && trueIndex >= lastStringIndex && substringIndex != -1 {
			lastStringIndex = trueIndex
			lastStringValue = j + 1
		}
	}

	return lastStringValue, lastStringIndex
}

func reverseString(line string) string {
	runes := []rune(line)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}